'use strict';
var crawler = require('npm-license-crawler');
var jsonfile = require('jsonfile');
var _ = require('underscore');
var chalk = require('chalk');

function loadSettings(path) {
    try {
        return jsonfile.readFileSync(path + '/.solicitoriorc');
    } catch(e) {
        throw chalk.red('No solicitoriorc found in project root');
    }
}

function createLicenseRegex(licenses) {
    return new RegExp(licenses.join('|').toUpperCase());
}

function isCompatible(license, compatibleLicenses) {
    return license.licenses.toUpperCase().match(createLicenseRegex(compatibleLicenses));
}

function isWarninglicense(license, warningLicenses) {
    if (!warningLicenses || !warningLicenses.length) {
        return false;
    }
    return license.licenses.toUpperCase().match(createLicenseRegex(warningLicenses));
}

function getModuleName(moduleName) {
    const splitted = moduleName.split('@');
    if(splitted.length > 1) {
        return splitted[0];
    } else {
        return moduleName;
    }
}

function isWhitelisted(license, whitelist, ignoreVersions) {
    if (ignoreVersions) {
        return !!_.some(whitelist, moduleName => moduleName.startsWith(getModuleName(license.moduleName)));
    } else {
        return whitelist.indexOf(license.moduleName) > -1;
    }
}

module.exports = function (path, flags) {
    let settings = loadSettings(path);

    const options = {
        start: [path],
        json: 'licenses.json',
        unknown: false
    };

    if(flags.exclude && flags.exclude.length) {
        options.exclude = flags.exclude;
    }

    const compatibleLicenses = settings.compatibleLicenses.concat(settings.warningLicenses);
    const warningLicenses = settings.warningLicenses;
    const moduleWhitelist = settings.whiteListedModules;

    crawler.dumpLicenses(options,
        function(error, licenses){
            if (error) {
                console.error("Error:", error);
            } else {
                licenses = _.map(licenses, (license, moduleName) => _.extend(license, { moduleName }));

                const compatbileModules = _.filter(licenses, license => {
                    return isCompatible(license, compatibleLicenses) || isWhitelisted(license, moduleWhitelist, flags.ignoreVersions);
                });

                const warningModules = _.filter(licenses, license => isWarninglicense(license, warningLicenses));
                const totalModuleCount = licenses.length;
                const incompatbileModules = _.difference(licenses, compatbileModules);

                if(incompatbileModules.length) {
                    console.log(chalk.red(`Found ${incompatbileModules.length} out of ${totalModuleCount} modules incompatible for use by us`));
                    _.each(incompatbileModules, module => console.log(chalk.red(`${module.moduleName} ${module.licenses} ${module.licenseUrl}` )));
                } else {
                    console.log(chalk.green(`All ${totalModuleCount} licenses are compliant`));
                }

                console.log(chalk.yellow(`Found ${warningModules.length} licenses flagged as warning`));

                if(incompatbileModules.length) {
                    process.exit(1);
                }

                process.exit(0);
            }
        }
    );
};
